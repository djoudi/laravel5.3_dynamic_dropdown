# Laravel 5.3  Dynamic Drop down change from database with Ajax

Laravel 5.3  Dynamic Drop down change from database with Ajax.

- changing Product Name based on Product Category from database.
- changing Product Price based on Product Name from database.

Project youtube tutorial link 
======

>>>
Video Tutorial for this project can be found on https://www.youtube.com/watch?v=N5ctY9nPt9o 

Usage
======
    
 in **view**
 

  ```html
 <span>Product Category: </span>
	<select style="width: 200px" class="productcategory" id="prod_cat_id">
		
		<option value="0" disabled="true" selected="true">-Select-</option>
		@foreach($prod as $cat)
			<option value="{{$cat->id}}">{{$cat->product_cat_name}}</option>
		@endforeach

	</select>

	<span>Product Name: </span>
	<select style="width: 200px" class="productname">

		<option value="0" disabled="true" selected="true">Product Name</option>
	</select>

	<span>Product Price: </span>
	<input type="text" class="prod_price">
  ```
  
  
```script
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

```


```script
<script type="text/javascript">
	$(document).ready(function(){

		$(document).on('change','.productcategory',function(){
			// console.log("hmm its change");

			var cat_id=$(this).val();
			// console.log(cat_id);
			var div=$(this).parent();

			var op=" ";

			$.ajax({
				type:'get',
				url:'{!!URL::to('findProductName')!!}',
				data:{'id':cat_id},
				success:function(data){
					//console.log('success');

					//console.log(data);

					//console.log(data.length);
					op+='<option value="0" selected disabled>chose product</option>';
					for(var i=0;i<data.length;i++){
					op+='<option value="'+data[i].id+'">'+data[i].productname+'</option>';
				   }

				   div.find('.productname').html(" ");
				   div.find('.productname').append(op);
				},
				error:function(){

				}
			});
		});

		$(document).on('change','.productname',function () {
			var prod_id=$(this).val();

			var a=$(this).parent();
			console.log(prod_id);
			var op="";
			$.ajax({
				type:'get',
				url:'{!!URL::to('findPrice')!!}',
				data:{'id':prod_id},
				dataType:'json',//return data will be json
				success:function(data){
					console.log("price");
					console.log(data.price);

					// here price is coloumn name in products table data.coln name

					a.find('.prod_price').val(data.price);

				},
				error:function(){

				}
			});


		});

	});
</script>
```

 in **Controller**
```laravel

use App\ProductCat;
use App\Product;


public function prodfunct(){

		$prod=ProductCat::all();//get data from table
		return view('productlist',compact('prod'));//sent data to view

	}

	public function findProductName(Request $request){

		
	    //if our chosen id and products table prod_cat_id col match the get first 100 data 

        //$request->id here is the id of our chosen option id
        $data=Product::select('productname','id')->where('prod_cat_id',$request->id)->take(100)->get();
        return response()->json($data);//then sent this data to ajax success
	}


	public function findPrice(Request $request){
	
		//it will get price if its id match with product id
		$p=Product::select('price')->where('id',$request->id)->first();
		
    	return response()->json($p);
	}
```

in **Route**
```laravel
Route::get('/prodview','TestController@prodfunct');
Route::get('/findProductName','TestController@findProductName');
Route::get('/findPrice','TestController@findPrice');
```

Important directory in this project
======
- laravel5_dynamic_dropdown(projectname)
    - app
        - Http
            - controllers
                 - TestController.php (controller)
    - routes
         -web.php   
    - resources
        - views
             -productlist.blade.php  (view)
    - lara_dynamic_dropdown.sql (database)


